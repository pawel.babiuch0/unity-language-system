header=EpicVR LanguageSystem (pl)
polish=polski
english=angielski
fields-count=Liczba pól: {0}
multi-texts=Wiele tłumaczeń
change-original-text=Zmień oryginalny tekst
not-updated=Tekst przypisany przez Translator.TranslateText nie jest podmieniany.
change-me=Zmień mnie