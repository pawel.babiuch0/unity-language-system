﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace pl.EpicVR.Helpers
{
    [CustomEditor(typeof(DontDestroy))]
    public class DontDestroyEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GUI.color = Color.grey;
            GUILayout.Label("Do not destroy object on change scene");
        }
    }
}
