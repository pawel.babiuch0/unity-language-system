﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace pl.EpicVR.LanguageManager
{
    [CustomEditor(typeof(LoadLanguageSettings))]
    public class LoadLanguageSettingsEditor : Editor
    {
        private LoadLanguageSettings settings;

        private void OnEnable()
        {
            settings = (LoadLanguageSettings)target;
        }

        private void OnValidate()
        {
            if (settings.defaultLanguage.Equals(SystemLanguage.Unknown))
            {
                settings.defaultLanguage = SystemLanguage.English;
                Debug.LogWarning($"You can not use SystemLanguage.{SystemLanguage.Unknown} as default. Changed on SystemLanguage.{SystemLanguage.English}");
            }
        }

        public override void OnInspectorGUI()
        {
            GUILayout.Label("Load Language type:", EditorStyles.boldLabel);
            settings.loadType = (LoadType)EditorGUILayout.EnumPopup(settings.loadType);

            switch (settings.loadType)
            {
                case LoadType.FromFile:
                    GUILayout.Label("URL:", EditorStyles.boldLabel);
                    settings.url = GUILayout.TextField(settings.url, 100);
                    break;
            }

            GUILayout.Label("Default Language:", EditorStyles.boldLabel);
            settings.defaultLanguage = (SystemLanguage)EditorGUILayout.EnumPopup(settings.defaultLanguage);
            settings.overrideDefaultLanguageBySystemLanguage = GUILayout.Toggle(settings.overrideDefaultLanguageBySystemLanguage, "Override default language by system language");

            if (settings.overrideDefaultLanguageBySystemLanguage)
            {
                GUI.color = Color.grey;
                EditorGUILayout.LabelField($"System Language on THIS machine: {Application.systemLanguage}");
                GUI.color = Color.white;
            }
        }
    }
}
