﻿using pl.EpicVR.LanguageManager;
using System;
using UnityEngine;
using TMPro;
using System.Linq;

public class DemoController : MonoBehaviour
{
    [SerializeField] private LanguageManager languageManager = null;

    [Space, Header("UI")]
    [SerializeField] private TextMeshProUGUI txtFieldscount = null;
    [SerializeField] private TMP_Dropdown fieldsDropdown = null;

    private void Start()
    {
        string[] fields = languageManager.GetFieldsKeys;

        SetupFieldsCount(fields);
        SetupFieldsDropdown(fields);
    }

    public void OnDropdownChangeLanguage_ValueChanged(Int32 optionId)
    {
        switch (optionId)
        {
            case 0:
                languageManager.ChangeLanguage(SystemLanguage.Polish);
                break;
            case 1:
                languageManager.ChangeLanguage(SystemLanguage.English);
                break;
        }
    }

    public void OnFieldsDropdown_ValueChanged(TranslateTMP translator)
    {
        string newOriginalText = $"[{fieldsDropdown.options[fieldsDropdown.value].text}]";
        translator.ChangeOriginalText(newOriginalText);
    }

    private void SetupFieldsCount(string[] fields)
    {
        string translatedText = Translator.TranslateText("[fields-count]");
        txtFieldscount.text = string.Format(translatedText, fields.Length);
    }

    private void SetupFieldsDropdown(string[] fields)
    {
        fieldsDropdown.AddOptions(fields.ToList());
    }
}
