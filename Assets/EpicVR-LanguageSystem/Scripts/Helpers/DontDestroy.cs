﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pl.EpicVR.Helpers
{
    public class DontDestroy : MonoBehaviour
    {
        private void OnEnable()
        {
            // This method allow you to disable this script in inspector;
        }

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }
    }
}
