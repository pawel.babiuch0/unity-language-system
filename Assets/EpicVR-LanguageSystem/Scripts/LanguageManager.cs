﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace pl.EpicVR.LanguageManager
{
    /// <summary>
    /// Main class of language system.
    /// </summary>
    [RequireComponent(typeof(Helpers.DontDestroy))]
    public class LanguageManager : MonoBehaviour
    {
        /// <summary>
        /// Invoked in ChangeLanguage function.
        /// </summary>
        public static event Action onLanguageChange;
        private static Dictionary<string, string> Fields { get; set; }

        #region States
        private static readonly LoadFromState LOAD_FROM_RESOURCES_STATE = new LoadFromResources();
        private static readonly LoadFromState LOAD_FROM_URL_STATE = new LoadFromFile();
        private LoadFromState currentState;
        #endregion

        [SerializeField] private LoadLanguageSettings loadLanguageSettings = null;

        /// <summary>
        /// Get array of keys 
        /// </summary>
        public string[] GetFieldsKeys => Fields.Keys.ToArray();

        private void Awake()
        {
            SetupCurrentState();
            LoadLanguageFiels();
        }

        /// <summary>
        /// Find key to translate in fields dictionary
        /// </summary>
        /// <param name="key">text to translate (in square brackets)</param>
        /// <returns>translated key or return key (if field not found)</returns>
        public static string GetField(string key)
        {
            if (Fields.ContainsKey(key))
                return Fields[key];
            else
            {
                Debug.LogWarning($"Key '{key}' not found.");
                return key;
            }
        }

        /// <summary>
        /// Change your language.
        /// This function invoke onLanguageChange event
        /// </summary>
        /// <param name="newLanguage">New language to set</param>
        public void ChangeLanguage(SystemLanguage newLanguage)
        {
            string languageToLoad = GetLanguageLetters(newLanguage);
            Fields = currentState.LoadLanguage(languageToLoad);
            onLanguageChange.Invoke();
        }

        private void SetupCurrentState()
        {
            ((LoadFromFile)LOAD_FROM_URL_STATE).URL = loadLanguageSettings.url;

            switch (loadLanguageSettings.loadType)
            {
                case LoadType.FromResources:
                    currentState = LOAD_FROM_RESOURCES_STATE;
                    break;
                case LoadType.FromFile:
                    currentState = LOAD_FROM_URL_STATE;
                    break;
            }
        }

        private void LoadLanguageFiels()
        {
            Fields = new Dictionary<string, string>();

            try
            {
                SystemLanguage checkLanguage = (loadLanguageSettings.overrideDefaultLanguageBySystemLanguage || loadLanguageSettings.defaultLanguage.Equals(SystemLanguage.Unknown)) ? Application.systemLanguage : loadLanguageSettings.defaultLanguage;
                string languageToLoad = GetLanguageLetters(checkLanguage);
                Fields = currentState.LoadLanguage(languageToLoad);
            }
            catch (Exception e)
            {
                Fields = null;
                Debug.LogError($"@LoadLanguageFiels: {e.Message}");
            }
            finally
            {
                onLanguageChange?.Invoke();
            }
        }

        private string GetLanguageLetters(SystemLanguage checkLanguage)
        {
            string result = string.Empty;

            switch (checkLanguage)
            {
                case SystemLanguage.Afrikaans:
                    result = "af";
                    break;
                case SystemLanguage.Arabic:
                    result = "ar";
                    break;
                case SystemLanguage.Belarusian:
                    result = "by";
                    break;
                case SystemLanguage.Bulgarian:
                    result = "bg";
                    break;
                case SystemLanguage.Catalan:
                    result = "ca";
                    break;
                case SystemLanguage.Chinese:
                    result = "zh";
                    break;
                case SystemLanguage.Czech:
                    result = "cs";
                    break;
                case SystemLanguage.Danish:
                    result = "da";
                    break;
                case SystemLanguage.Dutch:
                    result = "nl";
                    break;
                case SystemLanguage.Estonian:
                    result = "et";
                    break;
                case SystemLanguage.Faroese:
                    result = "fo";
                    break;
                case SystemLanguage.Finnish:
                    result = "fi";
                    break;
                case SystemLanguage.French:
                    result = "fr";
                    break;
                case SystemLanguage.German:
                    result = "de";
                    break;
                case SystemLanguage.Greek:
                    result = "el";
                    break;
                case SystemLanguage.Hebrew:
                    result = "iw";
                    break;
                case SystemLanguage.Hungarian:
                    result = "hu";
                    break;
                case SystemLanguage.Icelandic:
                    result = "is";
                    break;
                case SystemLanguage.Indonesian:
                    result = "in";
                    break;
                case SystemLanguage.Italian:
                    result = "it";
                    break;
                case SystemLanguage.Japanese:
                    result = "ja";
                    break;
                case SystemLanguage.Korean:
                    result = "ko";
                    break;
                case SystemLanguage.Latvian:
                    result = "lv";
                    break;
                case SystemLanguage.Lithuanian:
                    result = "lt";
                    break;
                case SystemLanguage.Norwegian:
                    result = "no";
                    break;
                case SystemLanguage.Polish:
                    result = "pl";
                    break;
                case SystemLanguage.Portuguese:
                    result = "pt";
                    break;
                case SystemLanguage.Romanian:
                    result = "ro";
                    break;
                case SystemLanguage.Russian:
                    result = "ru";
                    break;
                case SystemLanguage.SerboCroatian:
                    result = "sh";
                    break;
                case SystemLanguage.Slovak:
                    result = "sk";
                    break;
                case SystemLanguage.Slovenian:
                    result = "sl";
                    break;
                case SystemLanguage.Spanish:
                    result = "es";
                    break;
                case SystemLanguage.Swedish:
                    result = "sv";
                    break;
                case SystemLanguage.Thai:
                    result = "th";
                    break;
                case SystemLanguage.Turkish:
                    result = "tr";
                    break;
                case SystemLanguage.Ukrainian:
                    result = "uk";
                    break;
                case SystemLanguage.Vietnamese:
                    result = "vi";
                    break;
                case SystemLanguage.ChineseSimplified:
                    result = "zh";
                    break;
                case SystemLanguage.ChineseTraditional:
                    result = "zh";
                    break;
                default:
                    result = "en";
                    break;
            }
            return result;
        }
    }
}
