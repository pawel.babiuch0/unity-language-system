﻿using UnityEngine;
using TMPro;
using System.Linq;
using System;

namespace pl.EpicVR.LanguageManager
{
    [RequireComponent(typeof(TMP_Dropdown))]
    public class TranslateDropdownTMP : Translator
    {
        private TMP_Dropdown dropdownTMP;
        private string[] originalTexts;

        private void Awake()
        {
            dropdownTMP = GetComponent<TMP_Dropdown>();
            originalTexts = dropdownTMP.options.Select(x => x.text).ToArray();
        }

        [Obsolete("This function is Obsolve for this class", true)]
        public override void ChangeOriginalText(string newText)
        {
            throw new Exception("This function is Obsolve for this class.");
        }

        /// <summary>
        /// Change original texts for dropdown TMP
        /// </summary>
        /// <param name="newTexts"></param>
        public void ChangeOriginalText(string[] newTexts)
        {
            for (int i = 0; i < originalText.Length || i < newTexts.Length; i++)
            {
                originalTexts[i] = newTexts[i];
            }

            Translate();
        }

        public override void Translate()
        {
            for (int i = 0; i < dropdownTMP.options.Count; i++)
            {
                dropdownTMP.options[i].text = TranslateText(originalTexts[i]);
            }

            dropdownTMP.RefreshShownValue();
        }
    }
}
