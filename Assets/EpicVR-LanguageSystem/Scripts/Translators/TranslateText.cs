﻿using UnityEngine;
using UnityEngine.UI;

namespace pl.EpicVR.LanguageManager
{
    [RequireComponent(typeof(Text))]
    public class TranslateText : Translator
    {
        private Text txtUI;

        private void Awake()
        {
            txtUI = GetComponent<Text>();
            base.originalText = txtUI.text;
        }

        public override void Translate()
        {
            txtUI.text = TranslateText(originalText);
        }
    }
}
