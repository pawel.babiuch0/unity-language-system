﻿using UnityEngine;
using TMPro;

namespace pl.EpicVR.LanguageManager
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TranslateTMP : Translator
    {
        private TextMeshProUGUI txtUI;

        private void Awake()
        {
            txtUI = GetComponent<TextMeshProUGUI>();
            base.originalText = txtUI.text;
        }

        public override void Translate()
        {
            txtUI.text = TranslateText(originalText);
        }
    }
}
