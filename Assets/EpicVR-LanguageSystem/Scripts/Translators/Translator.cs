﻿using UnityEngine;

namespace pl.EpicVR.LanguageManager
{
    public abstract class Translator : MonoBehaviour
    {
        /// <summary>
        /// Text that language system translate in this class
        /// </summary>
        protected string originalText = string.Empty;

        protected virtual void OnEnable()
        {
            LanguageManager.onLanguageChange += Translate;
            Translate();
        }

        protected virtual void OnDisable()
        {
            LanguageManager.onLanguageChange -= Translate;
        }

        /// <summary>
        /// Change original text and translate them
        /// </summary>
        /// <param name="newText">new original text</param>
        public virtual void ChangeOriginalText(string newText)
        {
            originalText = newText;
            Translate();
        }

        /// <summary>
        /// Translate original text
        /// </summary>
        public abstract void Translate();

        /// <summary>
        /// Translate text. Function search text in square brackets
        /// eg.: "[this-is-text-to-translate] and this is skipped"
        /// </summary>
        /// <param name="textToTranslate">Text you want to translate</param>
        /// <returns>Translated text</returns>
        public static string TranslateText(string textToTranslate)
        {
            int index = 0;
            int endIndex = 0;

            while ((index = textToTranslate.IndexOf('[')) >= 0)
            {
                endIndex = textToTranslate.IndexOf(']');
                string oldWord = textToTranslate.Substring(index +1, endIndex - index - 1);
                string newWord = LanguageManager.GetField(oldWord);

                textToTranslate = string.Concat(textToTranslate.Substring(0, index), newWord, textToTranslate.Substring(endIndex+1));
            }

            return textToTranslate;
        }
    }
}
