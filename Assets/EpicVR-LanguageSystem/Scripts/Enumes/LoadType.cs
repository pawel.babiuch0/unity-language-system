﻿namespace pl.EpicVR.LanguageManager
{
    public enum LoadType
    {
        FromResources, FromFile
    }
}
