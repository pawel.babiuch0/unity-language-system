﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pl.EpicVR.LanguageManager
{

    [CreateAssetMenu(fileName = "New LanguageManager Settings", menuName = "LanguageManager/Create LanguageManager Settings")]
    public class LoadLanguageSettings : ScriptableObject
    {
        public LoadType loadType = LoadType.FromResources;
        public SystemLanguage defaultLanguage = SystemLanguage.Polish;
        public bool overrideDefaultLanguageBySystemLanguage = true;
        public string url = string.Empty;
    }
}
