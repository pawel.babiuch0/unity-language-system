﻿using System;
using System.Collections.Generic;

namespace pl.EpicVR.LanguageManager
{
    public abstract class LoadFromState
    {
        public abstract Dictionary<string, string> LoadLanguage(string languageToLoad);

        protected void ConvertLinesToFields(string[] lines, out Dictionary<string, string> fields)
        {
            fields = new Dictionary<string, string>();
            int equalIndex;
            string key, value;

            foreach (string line in lines)
            {
                if ((equalIndex = line.IndexOf('=')) < 0 || line.StartsWith("#")) continue;

                key = line.Substring(0, equalIndex);
                value = line.Substring(equalIndex + 1, line.Length - equalIndex - 1).Replace("\\n", Environment.NewLine);

                fields.Add(key, value);
            }
        }
    }
}
