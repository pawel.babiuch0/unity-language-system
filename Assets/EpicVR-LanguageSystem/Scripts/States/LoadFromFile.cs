﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace pl.EpicVR.LanguageManager
{
    public class LoadFromFile : LoadFromState
    {
        /// <summary>
        /// URL with language files
        /// </summary>
        public string URL; // C:\Languages\

        public override Dictionary<string, string> LoadLanguage(string languageToLoad)
        {
            Dictionary<string, string> fields;
            string path = Path.Combine(URL, $"{languageToLoad}.txt");

            try
            {
                if (!File.Exists(path)) throw new Exception($"File {path} not found.");
                string[] lines = File.ReadAllLines(path, Encoding.UTF8);

                base.ConvertLinesToFields(lines, out fields);

            }
            catch (Exception e)
            {
                Debug.LogError($"@LoadLanguage: {e.Message}");
                fields = new Dictionary<string, string>();
            }

            Debug.Log($"@LoadLanguage: {fields.Count} fields loaded (lang: {languageToLoad}); (loadType: FromURL).");
            return fields;
        }
    }
}
