﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace pl.EpicVR.LanguageManager
{

    public class LoadFromResources : LoadFromState
    {
        private const string PATH = "EpicVR-LanguageSystem";

        public override Dictionary<string, string> LoadLanguage(string languageToLoad)
        {
            Dictionary<string, string> fields;

            try
            {
                TextAsset textAsset = Resources.Load($"{PATH}/{languageToLoad}") as TextAsset;
                if (textAsset == null) throw new Exception($"File Resources/{PATH}/{languageToLoad}.txt not found.");

                string file = textAsset.text;
                string[] lines = file.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

                base.ConvertLinesToFields(lines, out fields);

            }
            catch (Exception e)
            {
                Debug.LogError($"@LoadLanguage: {e.Message}");
                fields = new Dictionary<string, string>();
            }

            Debug.Log($"@LoadLanguage: {fields.Count} fields loaded (lang: {languageToLoad}); (loadType: FromResources).");
            return fields;
        }
    }
}