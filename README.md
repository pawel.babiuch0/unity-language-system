# unity-language-system


## O projekcie:

Moduł językowy, który umożliwia tłumaczenie aplikacji na wiele języków (w zależności, które zostały dodane do aplikacji) lub korzysta z Języka domyślnego (wybranego w trakcie projektowania aplikacji). Moduł umożliwia wczytywanie Języków z folderu [Resources](https://docs.unity3d.com/Manual/BestPracticeUnderstandingPerformanceInUnity6.html) lub ładowanie z dowolnego miejscan a dysku. Sposób ustawienia zaciągania Języka oraz wybór domyślnego Języka jest tworzony za pomocą [ScriptableObject](https://docs.unity3d.com/Manual/class-ScriptableObject.html), które podpminamy do głównego systemu językowego. System umożliwia nadpisanie domyślnego języka, Językiem systemu, na którym aplikacja została uruchomiona.

<img src='https://gitlab.com/pawel.babiuch0/unity-language-system/-/raw/master/Zrzuty/Ustawienia%20J%C4%99zyku.PNG' align='center' alt='Domyślne ustawienia językowe' />

Aplikacja umożliwia tłumaczenie każdego tekstu, dropdownlisty, inputfielda, dodawanie własnych tłumaczeń. Kilka zrzutów pokazujących proste demo:

<img src='https://gitlab.com/pawel.babiuch0/unity-language-system/-/raw/master/Zrzuty/app-view-pl.PNG' align='center' alt='Aplikacja versja pl' />
<img src='https://gitlab.com/pawel.babiuch0/unity-language-system/-/raw/master/Zrzuty/app-view-en.PNG' align='center' alt='Aplikacja wersja en' />


## Wykorzystane technologie:

- [Unity 2019.3](https://unity.com/)

## Dokumentacja projektu:

- [Dokumentacja PL](https://gitlab.com/pawel.babiuch0/unity-language-system/-/blob/master/Assets/EpicVR-LanguageSystem/Documentation-PL.pdf)


## Linki do projektu:

 - [LanguageSystem Build.zip](https://drive.google.com/open?id=1wyHGuRn9Y-CBBtKd3HVGTEoH1iuXcnJt)
 - [language-system.unitypackage](https://drive.google.com/open?id=1yYaWLSMd38X_9L-h-5MPzt73ioefagiu)